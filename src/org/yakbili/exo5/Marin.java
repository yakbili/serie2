package org.yakbili.exo5;

public  class Marin {  
	    String nom, prenom ;
	    int salaire ;
		
	    public Marin (String nouveauNom, String nouveauPrenom,  int nouveauSalaire) {  
	        nom = nouveauNom ;   
	        prenom = nouveauPrenom ;   
	        salaire = nouveauSalaire ; 
	        	        
	    }
	    
	     public Marin (String nom,  int nouveauSalaire) {     
	    	this.prenom =  "" ;   
	        salaire = nouveauSalaire ;   
	    }
	     
	    public void setNom(String nom) {
	    	this.nom = nom;
	    	System.out.println("Nom : " + nom);
	    }
	    
	    public String getNom() {
	    	return this.nom;
	    }
	    
	    public void setPrenom(String prenom) {
	    	this.prenom = prenom;
	    	System.out.println("Prenom : " + prenom);
	    }
	    
	    public String getPrenom() {
	    	return this.prenom;
	    }
	    
	    @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((nom == null) ? 0 : nom.hashCode());
			result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
			result = prime * result + salaire;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Marin other = (Marin) obj;
			if (nom == null) {
				if (other.nom != null)
					return false;
			} else if (!nom.equals(other.nom))
				return false;
			if (prenom == null) {
				if (other.prenom != null)
					return false;
			} else if (!prenom.equals(other.prenom))
				return false;
			if (salaire != other.salaire)
				return false;
			return true;
		}

		public void setSalaire(int salaire) {
	    	this.salaire = salaire;
	    	System.out.println("Salaire : " + salaire);
	    }
	    
	    public int getSalaire() {
	    	return this.salaire;
	    }
	    
	     public  void augmenteSalaire (int augmentation) {  
	        salaire = salaire + augmentation ;   
	    }	 
	    
	}
