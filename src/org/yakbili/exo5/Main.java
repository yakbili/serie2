package org.yakbili.exo5;

public class Main {

	public static void main(String[] args) {
		Marin m1 = new Marin(null, 0);
		Marin m2 = new Marin(null, 0);
		Marin m3 = new Marin(null, 0);
		
		m1.setNom("bar");
		m1.setPrenom("pagre");
		m1.setSalaire(2000);
		
		m2.setNom("bar");
		m2.setPrenom("pagre");
		m2.setSalaire(2000);
		
		m3.setNom("merou");
		m3.setPrenom("sar");
		m3.setSalaire(3000);
		
		System.out.println("m1=m2 au sens de equals ? " + m1.equals(m2));
		System.out.println("m3=m1 au sens de equals ? " + m3.equals(m1));
		System.out.println("m3=m2 au sens de equals ? " + m3.equals(m2));

		System.out.println("hashCode de m1 : " + m1.hashCode());
		System.out.println("hashCode de m2 : " + m2.hashCode());
		System.out.println("hashCode de m2 : " + m3.hashCode());
		

	
		
		
	}

}
