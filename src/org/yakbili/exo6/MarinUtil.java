package org.yakbili.exo6;

//import org.yakbili.exo5.Marin;

public class MarinUtil {
	public void augmenteSalaire(Marin[] marins, int pourcentage) {
		for(int i = 0; i<marins.length; i++) {
			marins[i].setSalaire(marins[i].getSalaire()*((100+pourcentage)/100));
		}
	}
	public int getMaxSalaire(Marin[] marins) {
		int max = 0;
		for(int i = 0; i<marins.length; i++) {
			if(marins[i].getSalaire()>=max){
				max = marins[i].getSalaire();
			}
		}
		return max;
}
	
	public int getMoyenneSalaire(Marin[] marins) {
		int salaires = 0;
		for(int i = 0; i<marins.length; i++) {
			salaires = salaires + marins[i].getSalaire();
		}
		return salaires/marins.length;
			
		}

		
}
	


